;;; words-list-generator.el --- Most common words from a given text -*- lexical-binding: t -*-

;; Author: Manolito
;; Version: 0.1.0
;; Keywords: tools

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; Produce a 1000 most used words files from a given eBook from
;; Gutenberg project on plain text.  This file can be easily imported
;; into `typit' as custom dictionary.  Please, refer to its own
;; documentation for more information.
;;
;; It will produce a file with a name like
;; 'dictionary.<name>.<language>.txt'.  For instance
;; `dictionary.quijote.es.txt' might be a suitable file name for a
;; dictionary based on most common words used on Miguel de Cervantes
;; Saavedra's Don Quijote, Spanish language.  The content of this file
;; are the 1000 most common words used on the source book.  Each word
;; on a new line and sorted descendant by its occurrence.
;;
;; Installation:
;;
;; To install it, checkout the file `words-list-generator.el' on your a
;; folder on your load-path or add your add it.
;;
;; (add-to-list load-path "/path/to/words-list-generator folder/")
;;
;; Usage:
;;
;; To produce your dictionary file you will need in hand a source
;; eBook from Gutenberg project on txt format and call
;; `words-list-generator-make-dictionary' as follows:
;; (words-list-generator-make-dictionary "/path/to/your/ebook.txt"
;; "language")
;;
;; `language' will be appended to your dictionary file name so choose
;; the language that suits your dictionary.  The dictionary will be
;; produced on the same source eBook folder.
;;
;; From command-line use the following command:
;;
;; emacs -Q --batch --load=/path/to/words-list-generator/words-list-generator.el \
;;   --eval='(words-list-generator-make-dictionary "/path/to/your/ebook.txt" "language")'
;;


;;; Code:

(defun words-list-generator--get-list-from-string (string)
  "Return STRING into a spitted list of words."
  (split-string string "[[:space:][:punct:][:digit:][:blank:][:cntrl:]]+" t))

(defun words-list-generator--get-plist (word-list)
  "Return alist made of WORD-LIST."

  (let ((plist (list)))
    (mapc (lambda (elt)
              (let ((word-count (lax-plist-get plist elt)))
                (when (null word-count)
                  (setq word-count 0))
                (setq plist (lax-plist-put plist elt (1+ word-count))))) word-list)
    plist))

(defun words-list-generator--plist-to-alist (plist)
  "Return PLIST as alist form."
  (let ((alist)
        (copy-plist (copy-sequence plist)))
    (while copy-plist
      (let* ((key (pop copy-plist))
             (value (pop copy-plist)))
        (push (cons key value) alist)))
    alist))

(defun words-list-generator--sort-plist (plist)
  "Return a ordered alist produced from PLIST.
Order descendant by number of occurrences."
  (sort (words-list-generator--plist-to-alist plist)
        (lambda (elt1 elt2)
          (> (cdr elt1) (cdr elt2)))))

(defun words-list-generator-make-dictionary (file-name language)
  "Produce a common words file from FILE-NAME.  LANGUAGE included on file name.

The file pointed by FILE-NAME MUST BE a eBook from Gutenberg
library project on text format.

The produced dictionary will be located on the same folder as the
source file and the file name format will be like
`dictionary.<source base file name>.<language>.txt'."
  (let ((begin-content)
        (end-content)
        (content)
        (most-used-words))
    (with-temp-buffer
      (insert-file-contents file-name)
      (re-search-forward "***.START.OF.\\(THIS\\|THE\\).PROJECT.GUTENBERG.EBOOK*.***$" nil t)
      (forward-line 1)
      (setq begin-content (point))
      (re-search-forward "***.END.OF.\\(THIS\\|THE\\).PROJECT.GUTENBERG.EBOOK*.***$" nil t)
      (forward-line -1)
      (setq end-content (point))
      (setq content (buffer-substring-no-properties begin-content end-content))
      (kill-this-buffer))
   (message "Processing...")
   (setq most-used-words (words-list-generator--sort-plist
     (words-list-generator--get-plist
      (words-list-generator--get-list-from-string content))))
   (message "Done")
   (with-temp-file
       (format "%sdictionary.%s.%s.txt" (file-name-directory file-name) (file-name-base file-name) language)
     ;; (setq-local  buffer-file-coding-system 'utf-8-unix)
     (dotimes (_index 1000)
       (let ((word (pop most-used-words)))
         (insert (format "%s\n" (car word))))))))

(provide 'words-list-generator)

;;; words-list-generator.el ends here
