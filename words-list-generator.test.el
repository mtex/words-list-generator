;;; words-list-generator.test.el --- Test suit for words-list-generator -*- lexical-binding: t -*-

;; Author: Manolito
;; Version: 0.1.0
;; Keywords: testing
;; Package-Requires: ((ert) (words-list-generator "0.1.0"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; This package try to be a test suit for `words-list-generator'
;; development and quality.  For the most part is pretty content
;; agnostic but never the less it uses some Miguel de Cervantes
;; Saavedra's Don Quijote excerpts from Project Gutenberg ebook.
;; Which was the main reason to develop the tool.
;;
;; To run tests `words-list-generator.el` and
;; `words-list-generator.test.el' must be loaded.  Tests can be run
;; interactively `M-x ert RET t' or by command line as follow:
;;
;; `emacs -Q --batch --load=/path/to/words-list-generator/words-list-generator.el \
;;   --load=/path/to/words-list-generator/words-list-generator.test.el --eval="(ert t)"'
;;

;;; Code:

(require 'words-list-generator)
(require 'ert)

(ert-deftest should-produce-list-of-words-from-string ()
  "Should be able to extract a list of words.
Given a common text string should be able to extract a list of
words cleaning out any white-space, punctuation, control character
whatsoever."
  (let ((test-string "This should produce a, list: of; words. — —dijo Quijote— ¿qué »"))
    (let ((result (words-list-generator--get-list-from-string test-string)))
      (should (not (null result)))
      (should (listp result))
      (mapc (lambda (word)
              (should (stringp word))
              (should-not (string-match "[[:space:][:punct:][:digit:][:blank:]]+" word))) result)
      (should (member "words" result))
      (should (member "of" result))
      (should (member "list" result))
      (should (member "a" result))
      (should-not (member "—" result))
      (should-not (member "—dijo" result))
      (should-not (member "Quijote—" result))
      (should-not (member "¿qué" result))
      (should-not (member "»" result)))))

(ert-deftest should-produce-plist-from-word-list ()
  "Should produce a PLIST from a LIST of words.
PLIST holds each word count preventing word duplication."
  (let ((test-list (list "word1" "word2" "word3")))
    (let ((result (words-list-generator--get-plist test-list)))
      (should (not (null result)))
      (should (not (null (lax-plist-get result "word1"))))
      (should (equal 1 (lax-plist-get result "word1"))))))

(ert-deftest should-count-words ()
  "Given a list of words should count words occurrence.
Given a list of words, keep word occurrence counting on a PLIST."
  (let ((test-list (list "word1" "word1" "word2")))
    (let ((result (words-list-generator--get-plist test-list)))
      (should (equal 1 (lax-plist-get result "word2")))
      (should (equal 2 (lax-plist-get result "word1"))))))

(ert-deftest should-produce-alist-from-plist ()
  "Should produce a ALIST from a PLIST.
Produce a ALIST structure from PLIST with word-count pair.  ALIST
allows sorting and slicing operations easier than PLIST."
  (let* ((test-plist (list "word1" 1 "word2" 2))
         (copy-plist (copy-sequence test-plist)))
    (let ((result (words-list-generator--plist-to-alist test-plist)))
      (should (equal (list (cons "word1" 1) (cons "word2" 2))
                     (sort result (lambda (lt gt) (string< (car lt) (car gt))))))
      (should (equal test-plist copy-plist)))))

(ert-deftest should-sort-word-count ()
  "Should sort words by occurrence ascendant."
  (let ((test-plist (list "word1" 1 "word2" 2 "word3" 3)))
    (let ((result (words-list-generator--sort-plist test-plist)))
      (let ((item (nth 0 result) ))
        (should (equal 3 (cdr item)))
        (should (equal "word3" (car item))))
      (let ((item (nth 1 result)))
        (should (equal 2 (cdr item)))
        (should (equal "word2" (car item))))
      (let ((item (nth 2 result)))
        (should (equal 1 (cdr item)))
        (should (equal "word1" (car item)))))))

(ert-deftest should-produce-a-quijote-dictionary ()
  "Should produce a Don Quijote ES dictionary.
Produce a Don Quijote ES dictionary from a Don Quijote excerpt
from Gutenberg library project.  No encoding conversion issues on
dictionary production operation.

For testing this feature it is needed a external resource"
  (let* ((testing-directory (file-name-directory (symbol-file 'should-produce-a-quijote-dictionary 'ert--test)))
         (dictionary-file-name (concat
                                testing-directory
                                  "dictionary.quijote.test.es.txt"))
        (file-name (concat testing-directory "quijote.test.txt")))
    (unwind-protect
        (progn
          (words-list-generator-make-dictionary file-name "es")
          (should (file-exists-p dictionary-file-name))
          (with-temp-buffer
            (insert-file-contents dictionary-file-name)
            (let ((file-content (buffer-string)))
              (should-not (string-match-p "—" file-content))
              (should-not (string-match-p "—dijo" file-content))
              (should-not (string-match-p "Quijote—" file-content))
              (should-not (string-match-p "¿qué" file-content))
              (should-not (string-match-p "¡oh" file-content)))))
      (delete-file dictionary-file-name))))

(provide 'words-list-generator.test)

;;; words-list-generator.test.el ends here
